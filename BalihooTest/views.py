from django.shortcuts import render
from django.http import HttpResponse
from PuzzleSolver import PuzzleSolver


def home(request):
    if not 'q' in request.GET:
        return render(request, 'profile.html')
    if request.GET['q'] == "Ping":
        return HttpResponse("OK")
    elif request.GET['q'] == "Email Address":
        return HttpResponse("stevezundel@gmail.com")
    elif request.GET['q'] == "Phone":
        return HttpResponse("814-380-9635")
    elif request.GET['q'] == "Degree":
        return HttpResponse("Masters Software Engineering Penn State University 2013, \r\n" \
               "BS Electrical Engineering Brigham Young University 2009")
    elif request.GET['q'] == "Years":
        return HttpResponse("4")
    elif request.GET['q'] == "Position":
        return HttpResponse("Software Engineer")
    elif request.GET['q'] == "Source":
        return HttpResponse("https://bitbucket.org/stevezundel/balihootest/src")
    elif request.GET['q'] == "Puzzle":
        puzzle_solver = PuzzleSolver(request.GET['d'])
        return HttpResponse(puzzle_solver.puzzle())
    elif request.GET['q'] == "Resume":
        return HttpResponse("http://104.236.200.25")
    elif request.GET['q'] == "Name":
        return HttpResponse("Steven Zundel")
    elif request.GET['q'] == "Referrer":
        return HttpResponse("Indeed.com")
    elif request.GET['q'] == "Status":
        return HttpResponse("Yes")



def profile(request):
    return render(request, 'profile.html')