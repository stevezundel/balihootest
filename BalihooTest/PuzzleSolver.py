from copy import copy


class PuzzleSolver:
    def __init__(self, abcd):
        self.quartets = self.__setup_quartets(abcd[len("Please solve this puzzle:\nABCD\n") + 1:])

    def puzzle(self):
        self.__set_first_column()
        self.__add_equals_signs()
        self.__mirror()
        self.__fill_in_gaps()
        answer = " ABCD\r\n"
        columns = "ABCD"
        for index in range(len(self.quartets)):
            answer += columns[index]
            for character in self.quartets[index]:
                answer += character
            if index < len(self.quartets):
                answer += "\r\n"
        return answer

    @staticmethod
    def __setup_quartets(abcd):
        check_indices = (range(1, 5))
        quartet_length = len(check_indices)
        check_indices.extend(range(7, 11))
        check_indices.extend(range(13, 17))
        check_indices.extend(range(19, 23))
        quartets = []
        quartet = []
        for i in range(len(check_indices)):
            if i % quartet_length == 0:
                quartet = []
            quartet.extend(abcd[check_indices[i]])
            if len(quartet) == quartet_length:
                quartets.append(quartet)
        return quartets

    def __set_first_column(self):
        column_values = []
        column_index = -1
        for i in range(len(self.quartets)):
            for j in range(len(self.quartets[i])):
                if self.quartets[i][j] != '-':
                    column_values.extend(self.quartets[i][j])
                if self.quartets[i][j] == "=":
                    column_index = i
        for i in range(len(self.quartets)):
            self.quartets[i][column_index] = column_values[i]

    def __add_equals_signs(self):
        for i in range(len(self.quartets)):
            self.quartets[i][i] = "="

    def __mirror(self):
        for quartet_index in range(len(self.quartets)):
            for symbol_index in range(len(self.quartets[quartet_index])):
                if self.quartets[quartet_index][symbol_index] != '-' and self.quartets[quartet_index][symbol_index] != '=':
                    self.quartets[symbol_index][quartet_index] = self.__opposite_char(self.quartets[quartet_index][symbol_index])
        return self.quartets

    def __fill_in_gaps(self):
        row_dictionary = {}
        for index in range(len(self.quartets)):
            row_dictionary[index] = self.__row_symbols(self.quartets[index])
        greater_thans = len(self.quartets) - 1
        less_thans = 0
        rows = []
        for i in range(greater_thans + 1):
            row = [(greater_thans - i), (less_thans + i)]
            rows.append(row)
        return self.__gaps(row_dictionary, rows)

    def __is_complete(self):
        complete = True
        for quartet in self.quartets:
            if "-" in quartet:
                complete = False
                break
        return complete

    def __gaps(self, row_dictionary, remaining_rows):
        if self.__is_complete():
            return
        else:
            copy_dictionary = copy(row_dictionary)
            for key, row in copy_dictionary.iteritems():
                test_row = [row.GreaterThanCount, row.LessThanCount]
                if test_row in remaining_rows:
                    remaining_rows.pop(remaining_rows.index(test_row))
                    row_dictionary.pop(key, row)
            for checkRow in remaining_rows:
                possible_rows = []
                for key, row in row_dictionary.iteritems():
                    if checkRow[0] >= row.GreaterThanCount and checkRow[1] >= row.LessThanCount:
                        if self.quartets[key].count('-') > 1 and checkRow[0] > row.GreaterThanCount and checkRow[1] > row.LessThanCount:
                            break
                        else:
                            possible_rows.append((key, checkRow))
                if len(possible_rows) == 1:
                    char = "<"
                    if possible_rows[0][1][0] > row_dictionary[possible_rows[0][0]].GreaterThanCount:
                        char = ">"
                    for position, character in enumerate(self.quartets[possible_rows[0][0]]):
                        if character == "-":
                            self.quartets[possible_rows[0][0]][position] = char
                    remaining_rows.pop(remaining_rows.index(possible_rows[0][1]))
                    del row_dictionary[possible_rows[0][0]]
                    break
            self.__mirror()
            return self.__gaps(row_dictionary, remaining_rows)


    @staticmethod
    def __row_symbols(row):
        greater_than_count = 0
        less_than_count = 0
        for symbol in row:
            if symbol == ">":
                greater_than_count += 1
            elif symbol == "<":
                less_than_count += 1
        return SymbolCount(greater_than_count, less_than_count)

    @staticmethod
    def __opposite_char(symbol):
        if symbol == ">":
            return "<"
        elif symbol == "<":
            return ">"


class SymbolCount:
    def __init__(self, greater_than_count, less_than_count):
        self.GreaterThanCount = greater_than_count
        self.LessThanCount = less_than_count

    def __eq__(self, other):
        return self.GreaterThanCount == other.GreaterThanCount and self.LessThanCount == other.LessThanCount