from django.conf.urls import patterns, url
from BalihooTest.views import home
from views import profile


urlpatterns = patterns('',

    url(regex=r'^$', view=home, name='home_url'),
    url(r'^(?i)profile/', profile),
)
